package SeleniumTraining;

import java.util.Scanner;

public class Quiz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int score = 0;
		System.out.println("Are you ready for a quiz?");
		Scanner in = new Scanner(System.in);
		String userInput = in.nextLine();
		if (userInput.equals("Yes")) {
			System.out.println(
					"1. What is the capital of Alaska?" + " " + "Melbourne" + " " + "Anchorage" + " " + "Juneau");
			Scanner ques1 = new Scanner(System.in);
			String q1 = ques1.next();
			if (q1.equals("Juneau")) {
				System.out.println("That's right!");
				score++;
			} else {
				System.out.println("That's wrong answer!");
			}
			System.out.println("2. Can you store the value CAT in a variable of type int" + " " + "Yes" + " " + "No");
			Scanner ques2 = new Scanner(System.in);
			String q2 = ques2.next();
			if (q2.equals("Yes")) {
				System.out.println("Sorry CAT is a string. INT's can only store numbers");
			} else {
				System.out.println("That's right!");
				score++;
			}
			System.out.println("3. What is the result of 9+6/3" + " " + "5" + " " + "11" + " " + "15 / 3");
			Scanner ques3 = new Scanner(System.in);
			int q3 = ques3.nextInt();
			if (q3 == 11) {
				System.out.println("That's Correct");
				score++;
			} else {
				System.out.println("Sorry your answer is Incorrect!");
			}
			System.out.println("Final score is:" + score);

		} else {
			System.out.println("No Quiz Available for you");
		}
	}

}
