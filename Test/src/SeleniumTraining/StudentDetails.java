package SeleniumTraining;

import java.util.ArrayList;
import java.util.List;

public class StudentDetails {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Student> student = new ArrayList<Student>();
		Student s1 = new Student(001, "A", "Ash");
		Student s2 = new Student(002, "B", "Dan");
		Student s3 = new Student(003, "C", "Paul");
		Student s4 = new Student(004, "D", "Mike");
		Student s5 = new Student(005, "E", "Susan");
		student.add(s1);
		student.add(s2);
		student.add(s3);
		student.add(s4);
		student.add(s5);
		Printdetails(student);
	}

	private static void Printdetails(List<Student> student) {
		// TODO Auto-generated method stub
		for (Student studendetails : student) {
			System.out.println(" " + studendetails.sId + " " + studendetails.grade + " " + studendetails.name);
		}

	}
}
