package SeleniumTraining;

public class Conversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// a. Convert Integer 5 to Float
		int Int1 = 5;
		float Ifloat = Int1; // Automatic casting: integer to float
		System.out.println("Integer value is:" + Int1); // Outputs 5
		System.out.println("Float value is:" + Ifloat);

		// b. Convert Integer 5 to double
		int Int2 = 5;
		double Idouble = Int2; // Automatic casting: integer to double
		System.out.println("Integer value is:" + Int2); // Outputs 5.0
		System.out.println("Double value is:" + Idouble);

		// c. Convert Integer 5 to string
		int Int3 = 5;
		String Istring = Integer.toString(Int3); // Automatic casting: integer to string
		System.out.println("Integer value is:" + Int3); // Outputs 5
		System.out.println("String value is:" + Istring);

		// d. Convert float 2.585 to string
		float float1 = 2.585f;
		String Fstr = String.valueOf(float1); // Automatic casting: float to string
		System.out.println("Float value is:" + float1); // Outputs 2.585
		System.out.println("String value is:" + Fstr);

		// e. Convert float 2.585 to integer
		float float2 = 2.585f;
		int Fint = (int) float2; // Automatic casting: float to integer
		System.out.println("Float value is:" + float2); // Outputs 2
		System.out.println("Integer value is:" + Fint);

		// f. Convert float 2.585 to double
		float float3 = 2.585f;
		double Fdble = float3; // Automatic casting: float to double
		System.out.println("Float value is:" + float3); // Outputs 2.585
		System.out.println("Double value is:" + Fdble);

		// g. Convert double 1.000000000000000002585 to string
		double double1 = 1.000000000000000002585;
		String Dstr = String.valueOf(double1); // Automatic casting: double to string
		System.out.println("Double value is:" + double1); // Outputs 1.0
		System.out.println("String value is:" + Dstr);

		// h. Convert double 1.000000000000000002585 to float
		double double2 = 1.000000000000000002585;
		float Dflt = (float) (double2); // Automatic casting: double to float
		System.out.println("Double value is:" + double2); // Outputs 1.0
		System.out.println("Float value is:" + Dflt);

		// i. Convert double 1.000000000000000002585 to integer
		double double3 = 1.000000000000000002585;
		int Dint = (int) (double3); // Automatic casting: double to integer
		System.out.println("Double value is:" + double3); // Outputs 1
		System.out.println("Integer value is:" + Dint);

		// j. Convert string 1.000000000000000002585 to integer
		try {
			Double value = Double.parseDouble("1.000000000000000002585");
			System.out.println("Parsed double value is:" + value);
			int a = value.intValue();
			System.out.println("Parsed Int value " + a);

			// String str1 = Double.toString(d); // Automatic casting: double to string
			// System.out.println("String value is:" + str1); // Outputs 1
			// int Sint = Integer.parseInt(str1); // Automatic casting: string to integer
			// System.out.println("Integer value is:" + Sint);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// k. Convert string 1.000000000000000002585 to float
		String str2 = "1.000000000000000002585";
		float val = Float.parseFloat(str2);
		System.out.println("String value is:" + str2);
		System.out.println("Float value is:" + val); // Outputs 1.0

		// l. Convert string 1.000000000000000002585 to double0
		String str3 = "1.000000000000000002585";
		double Sdble = Double.parseDouble(str3);
		System.out.println("String value is:" + str3);
		System.out.println("Double value is:" + Sdble); // Outputs 1.0

	}

}
